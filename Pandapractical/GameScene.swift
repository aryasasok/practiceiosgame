//
//  GameScene.swift
//  Pandapractical
//
//  Created by Arya S  Asok on 2019-06-18.
//  Copyright © 2019 Parrot. All rights reserved.
//


import SpriteKit
import GameplayKit
var jumpButton:SKLabelNode!


class GameScene: SKScene {
    let coin = SKSpriteNode(imageNamed: "coin2")
    let panda = SKSpriteNode(imageNamed: "panda")
    let spikes = SKSpriteNode(imageNamed: "spikes")
    let jumpButton = SKSpriteNode(imageNamed: "JUMP")
    let pandaMovePointsPerSec: CGFloat = 480.0
    var velocity = CGPoint.zero
    var lastUpdateTime: TimeInterval = 0
    var dt: TimeInterval = 0
    
  
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    override func didMove(to view: SKView) {
        backgroundColor = SKColor.black
        let background = SKSpriteNode(imageNamed: "bakground")
        let jumpUpAction = SKAction.moveBy(x: 0, y:20 ,duration:0.2)

        background.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background.position = CGPoint(x: size.width/2, y: size.height/2)
        background.zPosition = -1
        addChild(background)
        addChild(coin)
        coin.setScale(3)
        coin.position = CGPoint(x: 600, y: 590)
        coin.zPosition = 200
        panda.position = CGPoint(x: 400, y: 450)
        panda.zPosition = 10
        addChild(panda)
        panda.setScale(0.5)
        addChild(spikes)
        spikes.position = CGPoint(x: 900, y: 390)
        spikes.zPosition = 10
        spikes.setScale(3)
        
        
        

    }
    
    override func update(_ currentTime: TimeInterval) {
        panda.position = CGPoint(x: panda.position.x + 8,
                                 y: panda.position.y)
    }
    func move(sprite: SKSpriteNode, velocity: CGPoint) {
        // 1
        let amountToMove = CGPoint(x: velocity.x * CGFloat(dt),
                                   y: velocity.y * CGFloat(dt))
        print("Amount to move: \(amountToMove)")
        // 2
        sprite.position = CGPoint(
            x: sprite.position.x + amountToMove.x,
            y: sprite.position.y + amountToMove.y)
    }
}
